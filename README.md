
 
Dans notre application happyness complète. Nous vous montrons comment
vous pouvez créer une belle architecture propre MVMV (model view model-view)
et un design épuré pour votre application de event électronique 
qui peut exécuter à la fois des appareils Andriod et iOS, 
car elle fonctionne avec flutter. Pour cela, on a utilisé 
le package GetX qui est une solution extra-légère et puissante 
pour Flutter. Il combine une gestion d'état (state management) 
haute performance, une injection de dépendance intelligente  et une gestion de route rapide et pratique.  
Pour la partie FRONT-END on a utilisé Flutter,
pour le BACK-END on a choisi de travailler avec FireBase
et sqflite en d’autres occasions.  

 

Screens it contains: 


SplashScreen 

LoginView 

RegisterView 

ModeView 

ReservationView 

ProfileView 

MyEventView 

HomeView 

DetailsView 

ControlView 

CategoryView 

 
Apres avoir fixé l’idée, on a choisi de consacrer une semaine (de 29/11 à 5/12) pour l’apprentissage de Flutter.   
Pour se faire, on a eu recours à UDEMY par l’une de ses formations
 intitulée : “ Flutter & Dart - The Complete Flutter App Development Course “   

De même, on a beaucoup appris de la documentation se trouvant sur
 le site de Flutter et du site Javatpoint.  

 

Bibliographie: 


https://github.com/jonataslaw/getx#about-get 

https://docs.flutter.dev/cookbook/persistence/sqlite 

https://firebase.flutter.dev/docs/overview 

https://www.youtube.com/watch?v=1ukSR1GRtMU&list=PL4cUxeGkcC9jLYyp2Aoh6hcWuxFDX6PBJ  

https://docs.flutter.dev/ 

https://www.youtube.com/watch?v=sfA3NWDBPZ4&list=PL4cUxeGkcC9j--TKIdkb3ISfRbJeJYQwC 
