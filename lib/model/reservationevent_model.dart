
class ReservationEventModel {
  String? name, image, price,eventId;
  int ? nbrplace;

  ReservationEventModel(
      {required this.name,
        required this.image,
        required this.price,
        required this.nbrplace,
        required this.eventId,
      });

  ReservationEventModel.fromJson(Map<dynamic, dynamic> map) {
    if (map == null) {
      return;
    }

    name = map['name'];
    image = map['image'];
    price = map['price'];
    nbrplace= map['nbrplace'];
    eventId= map['eventId'];
  }

  toJson() {
    return {
      'name': name,
      'image': image,
      'price': price,
      'nbrplace':nbrplace,
      'eventId':eventId,

    };
  }
}
