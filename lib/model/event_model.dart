
class EventModel {
  String? name, image, description, capacity, price,localisation,eventId,date;


  EventModel(
      {required this.name,
        required this.image,
        required this.description,
        required this.capacity,
        required this.localisation,
        required this.price,
        required this.date,
        required this.eventId});

  EventModel.fromJson(Map<dynamic, dynamic> map) {
    if (map == null) {
      return;
    }

    name = map['name'];
    image = map['image'];
    description = map['description'];
    capacity = map['capacity'];
    localisation = map['localisation'];
    price = map['price'];
    eventId = map['eventId'];
    date = map['date'];
  }

  toJson() {
    return {
      'name': name,
      'image': image,
      'description': description,
      'capacity': capacity,
      'localisation': localisation,
      'price': price,
      'eventId':eventId,
      'date':date
    };
  }
}
