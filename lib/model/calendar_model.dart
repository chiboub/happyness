import 'package:cloud_firestore/cloud_firestore.dart';

class CalendarModel {
  String? name, image, description, capacity, price,localisation,category;


  CalendarModel(
      {required this.name,
        required this.image,
        required this.description,
        required this.capacity,
        required this.localisation,
        required this.price,
        required this.category,
       });

  CalendarModel.fromJson(Map<dynamic, dynamic> map) {
    if (map == null) {
      return;
    }

    name = map['name'];
    image = map['image'];
    description = map['description'];
    capacity = map['capacity'];
    localisation = map['localisation'];
    price = map['price'];
    category=map['category'];

  }
   CalendarModel.fromDocumentSnapshot(QueryDocumentSnapshot snapshot) {
    //feild name should be exactly same as you given in friebase

    name = snapshot.get('name');
    category = snapshot.get('category');
    price = snapshot.get('price');
    localisation= snapshot.get('localisation');
    image=snapshot.get('image');
    description=snapshot.get('description');
  }
  toJson() {
    return {
      'name': name,
      'image': image,
      'description': description,
      'capacity': capacity,
      'localisation': localisation,
      'price': price,
      'category':category

    };
  }
}
enum EventCategory{ ALL, CONCERT, EDUCATION, SPORT, ART }