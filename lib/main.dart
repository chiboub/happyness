import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:happyness/core/view_model/reservation_view_model.dart';
import 'package:happyness/helper/binding.dart';
import 'package:happyness/view/auth/login_view.dart';
import 'package:happyness/view/auth/register_view.dart';
import 'package:happyness/view/categroy_view.dart';
import 'package:happyness/view/control_view.dart';
import 'package:happyness/view/homee_view.dart';
import 'package:happyness/view/profile_view.dart';
import 'package:happyness/view/reservation_view.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  Get.put(ReservationViewModel());
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      initialBinding: Binding(),
      home: Scaffold(
        body: ReservationView(),
      ),
      theme: ThemeData(
        fontFamily: 'SourceSans',
      ),
    );
  }
}