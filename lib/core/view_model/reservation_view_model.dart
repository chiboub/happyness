import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:happyness/core/service/database/reservation_database_helper.dart';
import 'package:happyness/model/reservationevent_model.dart';

class ReservationViewModel extends GetxController {
  ValueNotifier<bool> get loading => _loading;
  ValueNotifier<bool> _loading = ValueNotifier(false);
  List<ReservationEventModel> _reservationEventModel = [];

  List<ReservationEventModel> get reservationEventModel => _reservationEventModel;

  double get totalPrice => _totalPrice;
  double _totalPrice = 0.0;
  var dbHelper = ReservationDatabaseHelper.db;


  ReservationViewModel() {
    getAllEvent();

  }

  getAllEvent() async {
    _loading.value = true;
    _reservationEventModel = await dbHelper.getAllEvent();
    _loading.value = false;
    print(_reservationEventModel.length);
    getTotalPrice();
    update();
  }
  getTotalPrice() {
    for (int i = 0; i < _reservationEventModel.length; i++) {
      _totalPrice += (double.parse(_reservationEventModel[i].price!) *
          _reservationEventModel[i].nbrplace!);
      print(_totalPrice);
      update();
    }
  }

  addEvent(ReservationEventModel reservationEventModel) async {
    for (int i = 0; i < _reservationEventModel.length; i++) {
      if (_reservationEventModel[i].eventId == reservationEventModel.eventId) {
        return;
      }
    }
    await dbHelper.insert(reservationEventModel);
    _reservationEventModel.add(reservationEventModel);
    _totalPrice += (double.parse(reservationEventModel.price!) *
        reservationEventModel.nbrplace!);
    update();
  }
increaseQuantity(int index) async {
    _reservationEventModel[index].nbrplace = _reservationEventModel[index].nbrplace! + 1;
    _totalPrice += (double.parse(_reservationEventModel[index].price!) );
    // await dbHelper.updateEvent(_reservationEventModel[index]);

    update();
}
  decreaseQuantity(int index) async {
    _reservationEventModel[index].nbrplace = _reservationEventModel[index].nbrplace! - 1;
    _totalPrice -= (double.parse(_reservationEventModel[index].price!) );
     //await dbHelper.updateEvent(_reservationEventModel[index]);
    update();
  }
}