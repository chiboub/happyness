

import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:happyness/core/service/home_services.dart';
import 'package:happyness/model/category_model.dart';
import 'package:happyness/model/event_model.dart';


class HomeViewModel extends GetxController {
  ValueNotifier<bool> get loading => _loading;
  ValueNotifier<bool> _loading = ValueNotifier(false);

  List<CategoryModel> get categoryModel => _categoryModel;
  List<CategoryModel> _categoryModel = [];

  List<EventModel> get eventModel => _eventModel;
  List<EventModel> _eventModel = [];

  HomeViewModel() {
    getCategory();
    getBestSellingEvents();
  }

  getCategory() async {
    _loading.value = true;
    HomeService().getCategory().then((value) {
      for (int i = 0; i < value.length; i++) {
        _categoryModel.add(CategoryModel.fromJson(value[i].data() as Map<dynamic, dynamic>));
        _loading.value = false;
      }
      update();
    });
  }

  getBestSellingEvents() async {
    _loading.value = true;
    HomeService().getBestSelling().then((value) {
      for (int i = 0; i < value.length; i++) {
        _eventModel.add(EventModel.fromJson(value[i].data() as Map<dynamic, dynamic>));
        _loading.value = false;
      }
      print(_eventModel.length);
      update();
    });
  }
}
