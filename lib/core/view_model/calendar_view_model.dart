
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:happyness/model/calendar_model.dart';
import 'package:happyness/core/service/calendar_services.dart';


class CalendarViewModel extends GetxController {
  ValueNotifier<bool> get loading => _loading;
  ValueNotifier<bool> _loading = ValueNotifier(false);

  List<CalendarModel> get calendarModel => _calendarModel;
  List<CalendarModel> _calendarModel = [];

  CalendarViewModel() {
    getCalendar();
  }

  getCalendar() async {
    _loading.value = true;
    CalendarServices().getCalendar().then((value) {
      for (int i = 0; i < value.length; i++) {
        _calendarModel.add(CalendarModel.fromJson(value[i].data() as Map<dynamic, dynamic>));
        _loading.value = false;
      }
      update();
    });
  }

}

