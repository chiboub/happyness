import 'package:happyness/constance.dart';
import 'package:happyness/model/reservationevent_model.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

class ReservationDatabaseHelper{
  ReservationDatabaseHelper._();

  static final ReservationDatabaseHelper db = ReservationDatabaseHelper._();
  static Database? _database ;
  Future<Database?> get database async{
    if(_database!=null) return _database;
    _database = await initDb();
    return _database;
  }
  initDb() async{
    String path = join(await getDatabasesPath(),'ReservationEvent.db');
    return await openDatabase(path,
    version:  1 , onCreate: (Database db, int version) async{
      await db.execute('''
        CREATE TABLE $tableReservationEvent (
          $columnName TEXT NOT NULL,
          $columnPrice TEXT NOT NULL,
          $columnImage TEXT NOT NULL,
          $columnNbrPlace INTEGER NOT NULL,
          $columnEventId TEXT NOT NULL
        )
        ''');
      });
  }
  Future <List<ReservationEventModel>> getAllEvent() async{
    var dbClient = await database;
    List <Map> maps =await dbClient!.query(tableReservationEvent);
    List<ReservationEventModel> list = maps.isNotEmpty
        ? maps.map((event) => ReservationEventModel.fromJson(event)).toList()
    :[];
     return list;
  }

  insert(ReservationEventModel model) async{
    var dbClient = await database;
    await dbClient?.insert(tableReservationEvent, model.toJson(), conflictAlgorithm: ConflictAlgorithm.replace,);
  }
  
  updateEvent(ReservationEventModel model) async{
    var dbClient = await database;
    return await dbClient!.update(tableReservationEvent, model.toJson(),
    where: '$tableReservationEvent = ?' , whereArgs:  [model.eventId]);

  }
}