import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:happyness/model/user_model.dart';


class FireStoreUser {
  final CollectionReference _userCollectionRef =
      FirebaseFirestore.instance.collection('Users');
  FirebaseAuth _auth = FirebaseAuth.instance;
  Future<void> addUserToFireStore(UserModel userModel) async {
    return await _userCollectionRef
        .doc(userModel.userId)
        .set(userModel.toJson());
  }
  Future <DocumentSnapshot> getCurrentUser (String uid) async{
    return await _userCollectionRef.doc(uid).get();
  }
  Future<String> getCurrentUID() async {
    return (await _auth.currentUser!).uid;
  }

  // GET CURRENT USER
  Future getCurreentUser() async {
    return await _auth.currentUser!;
  }
}
