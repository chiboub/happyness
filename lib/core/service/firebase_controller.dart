import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:get/get_state_manager/src/simple/get_controllers.dart';
import 'package:get/get.dart';
import 'package:happyness/core/service/chip_controller.dart';
import 'package:happyness/model/calendar_model.dart';


class FirestoreController extends GetxController {
  //referance to firestore collection here laptop is collection name
  final CollectionReference _createCollectionRef =
  FirebaseFirestore.instance.collection('EventCreatedbyme');

  var EventList = <CalendarModel>[].obs;

  //dependency injection with getx
  ChipController _chipController = Get.put(ChipController());

  @override
  void onInit() {
    //binding to stream so that we can listen to realtime cahnges

    EventList.bindStream(
        getEvents(EventCategory.values[_chipController.selectedChip]));
    super.onInit();
  }

// this fuction retuns stream of laptop lsit from firestore

  Stream<List<CalendarModel>> getEvents(EventCategory category) {
    //using enum class LaptopBrand in switch case
    switch (category) {
      case EventCategory.ALL:
        Stream<QuerySnapshot> stream = _createCollectionRef.snapshots();
        return stream.map((snapshot) => snapshot.docs.map((snap) {
          return CalendarModel.fromDocumentSnapshot(snap);
        }).toList());
      case EventCategory.SPORT:
        Stream<QuerySnapshot> stream =
        _createCollectionRef.where('category', isEqualTo: 'sport').snapshots();
        return stream.map((snapshot) => snapshot.docs.map((snap) {
          return CalendarModel.fromDocumentSnapshot(snap);
        }).toList());

      case EventCategory.EDUCATION:
        Stream<QuerySnapshot> stream =
        _createCollectionRef.where('category', isEqualTo: 'education').snapshots();
        return stream.map((snapshot) => snapshot.docs.map((snap) {
          return CalendarModel.fromDocumentSnapshot(snap);
        }).toList());
      case EventCategory.CONCERT:
        Stream<QuerySnapshot> stream =
        _createCollectionRef.where('category', isEqualTo: 'concert').snapshots();
        return stream.map((snapshot) => snapshot.docs.map((snap) {
          return CalendarModel.fromDocumentSnapshot(snap);
        }).toList());
      case EventCategory.ART:
        Stream<QuerySnapshot> stream =
        _createCollectionRef.where('category', isEqualTo: 'art').snapshots();
        return stream.map((snapshot) => snapshot.docs.map((snap) {
          return CalendarModel.fromDocumentSnapshot(snap);
        }).toList());
    }
  }
}