import 'package:cloud_firestore/cloud_firestore.dart';

class CalendarServices {
  final CollectionReference _createCollectionRef =
  FirebaseFirestore.instance.collection('EventCreatedbyme');

  Future<List<QueryDocumentSnapshot>> getCalendar() async {
    var value = await _createCollectionRef.get();

    return value.docs;
  }
}