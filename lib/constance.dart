import 'package:flutter/material.dart';

const String tableReservationEvent ='ReservationEvent';
const String columnName = 'name';
const String columnImage = 'image';
const String columnNbrPlace = 'nbrplace';
const String columnPrice = 'price';
const String columnEventId = 'eventId';


const  primaryColor = Color.fromRGBO(144, 12, 63, 12);

const CACHED_USER_DATA = 'CACHED_USER_DATA';