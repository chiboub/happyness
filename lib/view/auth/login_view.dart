
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:happyness/core/view_model/auth_view_model.dart';
import 'package:happyness/view/auth/register_view.dart';
import 'package:happyness/view/widgets/custom_buttom.dart';
import 'package:happyness/view/widgets/custom_button_social.dart';
import 'package:happyness/view/widgets/custom_text.dart';
import 'package:happyness/view/widgets/custom_text_form_field.dart';

import '../../constance.dart';

class LoginView extends GetWidget<AuthViewModel> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      resizeToAvoidBottomInset: false,

      backgroundColor: Colors.white,
     
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.only(
            top: 70,
            right: 20,
            left: 20,
          ),
          child: Form(
            key: _formKey,
            child: Column(
              children: [
                Image.asset("assets/login.png",width: 170,),
                CustomText(text: "WELCOME BACK",
                 alignment: Alignment.center,
                  color: primaryColor,
                  fontSize: 30,
                  height: 2,
                ),
                SizedBox(
                  height: 30,
                ),
                CustomText(
                  text: 'Sign in to Continue',
                  fontSize: 14,
                  color: Colors.grey,
                ),
                SizedBox(
                  height: 15,
                ),
                CustomTextFormField(
                  text: 'Email',
                  hint: 'TAREK@gmail.com',
                  onSave: (value) {
                    controller.email = value!;
                  },
                  validator: (value) {
                    if (value == null) {
                      print("ERROR");
                    }
                  },
                ),
                SizedBox(
                  height: 20,
                ),
                CustomTextFormField(
                  text: 'Password',
                  hint: '**********',
                  onSave: (value) {
                    controller.password = value!;
                  },
                  validator: (value) {
                    if (value == null) {
                      print('error');
                    }
                  },
                ),
                SizedBox(
                  height: 20,
                ),

                SizedBox(
                  height: 15,
                ),
                CustomButton(
                  color1: primaryColor,
                  onPress: () {
                    _formKey.currentState!.save();

                    if (_formKey.currentState!.validate()) {
                            controller.signInWithEmailAndPassword();
                    }
                  },
                  text: 'Sign in',
                ),
                SizedBox(
                  height: 20,
                ),
                CustomText(
                  text: '-OR-',
                  alignment: Alignment.center,
                ),
                SizedBox(
                  height: 20,
                ),
                CustomButtonSocial(
                  text: 'Sign In with Google',
                  onPress: () {
                    controller.googleSignInMethod();
                  },
                  imageName: 'assets/google.png',
                ),
                SizedBox(
                  height: 15,
                ),
                CustomButton(
                  onPress: () {
                   Get.to(RegisterView());
                  },
                  text: 'Create account',
                  color2:Colors.blueGrey,
                  color1: Colors.grey.shade100,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
