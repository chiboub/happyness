
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:happyness/core/view_model/auth_view_model.dart';
import 'package:happyness/view/auth/login_view.dart';
import 'package:happyness/view/widgets/custom_buttom.dart';
import 'package:happyness/view/widgets/custom_text.dart';
import 'package:happyness/view/widgets/custom_text_form_field.dart';

import '../../constance.dart';

class RegisterView extends GetWidget<AuthViewModel> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.only(
            top: 70,
            right: 20,
            left: 20,
          ),
          child: Form(
            key: _formKey,
            child: Column(
              children: [
                Image.asset("assets/register.png",width: 160),
                CustomText(text: "CREATE YOUR ACCOUNT",
                  alignment: Alignment.center,
                  color: primaryColor,
                  fontSize: 27,
                  height: 2,
                ),
                const SizedBox(
                  height: 25,
                ),
                CustomTextFormField(
                  text: 'Name',
                  hint: 'TAREK',
                  onSave: (value) {
                    controller.name = value!;
                  },
                  validator: (value) {
                    if (value == null) {
                      print("ERROR");
                    }
                  },
                ),
                const SizedBox(
                  height: 20,
                ),
                CustomTextFormField(
                  text: 'Email',
                  hint: 'TAREK@gmail.com',
                  onSave: (value) {
                    controller.email = value!;
                  },
                  validator: (value) {
                    if (value == null) {
                      print("ERROR");
                    }
                  },
                ),
                const SizedBox(
                  height: 20,
                ),
                CustomTextFormField(
                  text: 'Password',
                  hint: '**********',
                  onSave: (value) {
                    controller.password = value!;
                  },
                  validator: (value) {
                    if (value == null) {
                      print('error');
                    }
                  },
                ),
                const SizedBox(
                  height: 50,
                ),
                CustomButton(
                  onPress: () {
                    _formKey.currentState?.save();
      
                    if (_formKey.currentState!.validate()) {
                      controller.createAccountWithEmailAndPassword();
                    }
                  },
                  text: 'Sign up',
                ),
                const SizedBox(
                  height: 20,
                ),
                CustomButton(
                  onPress: () {
                    Get.to(LoginView());
                  },
                  text: 'Already have an account',
                  color2:Colors.blueGrey,
                  color1: Colors.grey.shade100,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
