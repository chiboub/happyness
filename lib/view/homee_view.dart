import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:happyness/constance.dart';
import 'package:happyness/core/view_model/home_view_model.dart';
import 'package:happyness/model/event_model.dart';
import 'package:happyness/view/details_view.dart';
import 'package:happyness/view/myevent_view.dart';
import 'package:happyness/view/navigationdrawer.dart';
import 'package:happyness/view/widgets/custom_text.dart';

class HomeView extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return GetBuilder<HomeViewModel>(
      init: Get.put(HomeViewModel()),
      
      builder: (controller) => controller.loading.value
          ? const Center(child:  CircularProgressIndicator())
          : Scaffold(
        appBar: AppBar(
            title: const Center(child:  Text("HAPPY'NESS"),),
          backgroundColor: primaryColor,
        ),
        drawer: const NavigationDrawer(),

        body: SingleChildScrollView(
          child: Container(
            padding: const EdgeInsets.only(top: 20, left: 20, right: 20),
            child: Column(
              children: [
                _searchTextFormField(),
                const SizedBox(
                  height: 40,
                ),
                GestureDetector(
                  onTap: (){
                    Get.to(MyEventView());
                  },
                  child: CustomText(
                    text: "Categories",
                  ),
                ),
                const SizedBox(
                  height: 30,
                ),
                _listViewCategory(),
                const SizedBox(
                  height: 40,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    CustomText(
                      text: "Best Selling",
                      fontSize: 18,
                    ),
                    CustomText(
                      text: "Most Popular ",
                      fontSize: 16,
                    ),
                  ],
                ),
                const SizedBox(
                  height: 30,
                ),
                _listViewEvents(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _searchTextFormField() {
    return Container(

      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(20),
        color: Colors.grey.shade200,
      ),
      child: TextFormField(

        decoration: const InputDecoration(
          hintText: "Search",
          border: InputBorder.none,
          prefixIcon: Icon(
            Icons.search,
            color: Colors.black,
          ),
        ),
      ),
    );
  }

  Widget _listViewCategory() {
    return GetBuilder<HomeViewModel>(
      builder: (controller) => Container(
        color: Colors.grey.shade100,
        height: 100,
        child: ListView.separated(
          itemCount: controller.categoryModel.length,
          scrollDirection: Axis.horizontal,
          itemBuilder: (context, index) {
            return Column(
              children: [
                Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(50),
                    color: Colors.grey,
                  ),
                  height: 60,
                  width: 60,
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Image.network(controller.categoryModel[index].image as String,),
                  ),
                ),
                const SizedBox(
                  height: 20,
                ),
                CustomText(
                  text: controller.categoryModel[index].name.toString(),
                ),
              ],
            );
          },
          separatorBuilder: (context, index) => const SizedBox(
            width: 20,
          ),
        ),
      ),
    );
  }

   Widget _listViewEvents() {
    return GetBuilder<HomeViewModel>(
      builder: (controller) => Container(
        height: 550,
        child: ListView.builder(
          scrollDirection: Axis.vertical,
          physics: const BouncingScrollPhysics(),
          shrinkWrap: true,
          itemCount: controller.eventModel.length,
          itemBuilder: (context, index) {
            return GestureDetector(
                onTap: () {},
                child:  Column(
                  children: [
                    Card(
                        color: Colors.grey.shade200,
                        elevation: 20.0,
                        child: Column(
                          children: [

                            Container(
                                height: 200.0,
                                child:
                                Image.network(  "${controller.eventModel[index].image}",
                                  fit: BoxFit.cover,
                                  width: 500,
                                )
                            ),
                            ListTile(
                              title: Text("${controller.eventModel[index].name}"),
                              subtitle:Text(controller.eventModel[index].date.toString() ,),
                            ),
                            Container(
                              padding: const EdgeInsets.all(8.0),
                              alignment: Alignment.centerLeft,
                              child:Text("${controller.eventModel[index].description}"),
                            ),


                            Container(
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(20),
                                color: Colors.white24,
                              ),
                              alignment: Alignment.center,
                              child: TextButton(

                                child: const Text('SEE MORE'),
                                onPressed: () {
                                  Get.to(DetailsView(
                                    model: controller.eventModel[index],
                                  ));
                                },
                                
                              ),
                            )

                          ],
                        )),
                    const SizedBox(height: 20,)
                  ],

                )

            );

          },

        ),

      ),
    );
  }
}

