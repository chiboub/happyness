
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:happyness/constance.dart';
import 'package:happyness/core/view_model/reservation_view_model.dart';
import 'package:happyness/model/reservationevent_model.dart';
import 'package:happyness/model/event_model.dart';
import 'package:happyness/view/reservation_view.dart';
import 'package:happyness/view/widgets/custom_buttom.dart';
import 'package:happyness/view/widgets/custom_text.dart';

class DetailsView extends StatelessWidget {
  EventModel model;
  DetailsView({required this.model});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: Column(
          children: [
            GestureDetector(
            onTap: (){ Get.to(ReservationView());},
              child: Container(
                width: MediaQuery.of(context).size.width,
                height: 270,
                child: Image.network(
                  model.image as String,
                  fit: BoxFit.fill,
                ),
              ),
            ),
            const SizedBox(
              height: 15,
            ),
            Expanded(
              child: SingleChildScrollView(
                child: Container(
                  padding: EdgeInsets.all(18),
                  child: Column(
                    children: [
                      Row(
                        children: [
                          SizedBox(width: 10,),
                          CustomText(
                            text: model.name.toString(),
                            fontSize: 26,
                          ),
                        ],
                      ),
                      const SizedBox(
                        height: 15,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          Container(
                            padding: EdgeInsets.all(16),
                            width: MediaQuery.of(context).size.width * .4,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(20),
                                border: Border.all(
                                  color: primaryColor,
                                )),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: [
                                CustomText(
                                  text: 'Capcity:',
                                  color: primaryColor,
                                ),
                                CustomText(
                                  text: model.capacity.toString(),
                                ),
                              ],
                            ),
                          ),
                          Container(
                            padding: EdgeInsets.all(16),
                            width: MediaQuery.of(context).size.width * .44,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(20),
                                border: Border.all(
                                  color: primaryColor,
                                )),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: [
                                CustomText(
                                  text: 'Date:',
                                  color: primaryColor,
                                ),
                                 CustomText(
                                  text: model.date.toString(),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                      const SizedBox(
                        height: 25,
                      ),
                      Row(
                        children: [
                          CustomText(text: "Location :",color: primaryColor,),
                          SizedBox(width: 10,),
                          CustomText(
                            text: model.localisation.toString(),
                            fontSize: 18,
                          ),
                        ],
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      CustomText(text: "Details :",color: primaryColor,),

                      CustomText(
                        text: model.description.toString(),
                        fontSize: 18,
                        height: 2,
                      )
                    ],
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Column(
                    children: [
                      CustomText(
                        text: "PRICE ",
                        fontSize: 22,
                        color: primaryColor,
                      ),
                      CustomText(
                        text:  model.price.toString() + ' \DT',
                        color: Colors.black,
                        fontSize: 18,
                      )
                    ],
                  ),
                  GetBuilder<ReservationViewModel>(
                    init: Get.put(ReservationViewModel()),
                    builder: (controller)=> Container(
                     
                      width: 160,
                      height: 60,
                      child: CustomButton(
                        onPress:() => controller.addEvent(
                          ReservationEventModel(
                            name: model.name,
                            image: model.image,
                            price: model.price,
                            nbrplace: 1,
                              eventId:model.eventId
                          )
                         
                        ),
                        text: 'Add to Calendar',
                      ),
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
