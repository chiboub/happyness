
import'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:happyness/constance.dart';
import 'package:happyness/view/categroy_view.dart';
import 'package:happyness/view/myevent_view.dart';
import 'package:happyness/view/navigationdrawer.dart';
import 'package:happyness/view/widgets/custom_buttom.dart';

class ModeView extends StatelessWidget {
  const ModeView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blue.shade900,
      ),
      drawer:const NavigationDrawer(),
      body:SingleChildScrollView(
        child: Container(
            decoration: const BoxDecoration(
             image:  DecorationImage(
              image: AssetImage('assets/back1.png'),
             fit: BoxFit.cover,
        ),
        ),

          child: Padding(
            padding: const EdgeInsets.only(top: 500),
            child: Column(
              children: [

                const SizedBox(height: 45,),
                Container(
                  padding: const EdgeInsets.only(left: 10,right: 10),
                  child: CustomButton(
                    color1: primaryColor,
                    onPress: () {
                        Get.to(CategoryView());
                    },

                    text: 'Search for event',
                  ),
                ),
                const SizedBox(height: 15,),
                Container(
                  padding: const EdgeInsets.only(left: 10,right: 10),

                  child: CustomButton(

                    color1: primaryColor,
                    onPress: () {
                      Get.to(MyEventView());
                    },
                    text: 'Create new event',
                  ),
                ),
              ],
            ),
          ),


        ),
      ),
    );
  }
}
