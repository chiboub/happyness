import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
import 'package:happyness/constance.dart';
import 'package:happyness/core/view_model/reservation_view_model.dart';
import 'package:happyness/view/navigationdrawer.dart';
import 'package:happyness/view/widgets/custom_buttom.dart';
import 'package:happyness/view/widgets/custom_text.dart';

class ReservationView extends StatelessWidget {
 
  @override
  Widget build(BuildContext context) {
    return GetBuilder<ReservationViewModel>(
        init: Get.put(ReservationViewModel()),
    builder: (controller) =>  Scaffold(
      appBar: AppBar(
        backgroundColor: primaryColor,
        title: const Center(child: Text("HAPPY'NESS"),),
        ),
      drawer: const NavigationDrawer(),
      body: controller.reservationEventModel.length==0
          ? Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Image.asset(
              'assets/no_events.png'
          ),
          const SizedBox(height: 20,),
          CustomText(
            text: "NO EVENTS YET",
            fontSize: 25,
            color: Colors.grey.shade500,
            alignment: Alignment.topCenter,
          )
        ],
      )

            :Column(
        children: [
        Expanded(
          child: Container(
            
              child: ListView.separated(
                  itemBuilder: (context,index){
                    return Container(
                      height: 140,
                      child: Row(
                        children: [
                          Container(
                            width:150 ,
                              child: Image.network(
                                  controller.reservationEventModel[index].image as String,
                                fit: BoxFit.fill,

                              )),
                          Padding(
                            padding: const EdgeInsets.only(left: 30),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                const SizedBox(height: 13,),
                                CustomText(
                                  text: controller.reservationEventModel[index].name.toString(),
                                ),
                                const SizedBox(height: 6,
                                ),
                                CustomText(
                                  color: primaryColor,
                                  text: '${controller.reservationEventModel[index].price} \DT',
                                ),
                                const SizedBox(height: 20,
                                ),
                                Container(
                                  width: 140,
                                  color: Colors.grey.shade200,
                                  height: 40,
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      GestureDetector(
                                        child: const Icon(Icons.add,
                                          color:Colors.black
                                        ),
                                        onTap: (){
                                          controller.increaseQuantity(index);
                                        },
                                      ),
                                      const SizedBox(width: 20,),
                                      CustomText(
                                        text: controller.reservationEventModel[index].nbrplace.toString(),
                                      color: Colors.black,
                                      fontSize: 20,
                                      alignment: Alignment.center,
                                      ),
                                      GestureDetector(
                                        onTap: (){
                                          controller.decreaseQuantity(index);
                                        },
                                        child: Container(
                                          padding: const EdgeInsets.only(bottom: 20),
                                          child: const Icon(Icons.minimize,
                                              color:Colors.black
                                          ),

                                        ),
                                      ),
                                    ],
                                  ),
                                )
                              ],
                            ),
                          )
                        ],
                      ),
                    );
                  }, itemCount:controller.reservationEventModel.length,
                separatorBuilder: (BuildContext context, int index) {
                    return const SizedBox(height: 10,);
              } ,)
            ),

        ),
          Padding(
            padding: const EdgeInsets.only(left: 30,right: 30),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Column(
                    children:[
                      CustomText(
                        text: 'TOTAL',
                        fontSize: 22,
                        color: Colors.grey,
                      ),
                      const SizedBox(height: 10,),
                      GetBuilder<ReservationViewModel>(
                        init:  Get.find(),
                        builder: (controller)=>CustomText(
                          text: ' ${controller.totalPrice} \DT',
                          color: primaryColor,
                          fontSize: 18,

                        ),
                      ),
                    ]
                ),
                Container(
                 padding: const EdgeInsets.only(bottom:4 ),
                  alignment: Alignment.bottomRight,
                  height:60 ,
                  width: 230,
                  child: CustomButton(

                      onPress:(){/** CHEKOUT PAGE */} ,
                      text: 'Finaliser la Reservervation'
                      
                  ),
                )
              ],
            ),
          )
        ],
      ),
    ),
    );
  }
}
