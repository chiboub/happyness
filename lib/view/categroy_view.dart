
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:happyness/constance.dart';
import 'package:happyness/core/service/chip_controller.dart';
import 'package:happyness/core/service/firebase_controller.dart';
import 'package:happyness/model/calendar_model.dart';
import 'package:happyness/view/navigationdrawer.dart';

class CategoryView extends StatelessWidget {
  CategoryView({Key? key}) : super(key: key);

  final FirestoreController firestoreController =
  Get.put(FirestoreController());
  final ChipController chipController = Get.put(ChipController());

  //name of chips given as list
  final List<String> _chipLabel = ['All', 'concert', 'education', 'sport','art'];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
         backgroundColor: primaryColor,
        title: const Center(child: Text("HAPPY'NESS"),),
      ),
      drawer: const NavigationDrawer(),
        body: SafeArea(
          
          child: Container(
            color: Colors.grey.shade300,
            child: Column(

              children: [
                Obx(() => Wrap(

                    spacing: 10,
                    children: List<Widget>.generate(5, (int index) {
                      return ChoiceChip(

                        backgroundColor: Colors.grey.shade400,
                        label: Text(_chipLabel[index]),
                        selected: chipController.selectedChip == index,
                        onSelected: (bool selected) {
                          chipController.selectedChip = selected ? index : Null ;
                          firestoreController.onInit();
                          firestoreController.getEvents(
                              EventCategory.values[chipController.selectedChip]);
                        },
                      );
                    }),
                  ),
                ),
                Obx(() => Expanded(
                  child: ListView.builder(
                      scrollDirection: Axis.vertical,
                      physics: const BouncingScrollPhysics(),
                      shrinkWrap: true,
                      itemCount: firestoreController.EventList.length,
                      itemBuilder: (BuildContext context, int index) {
                        return Card(
                          color: Colors.grey.shade100,
                            elevation: 4.0,
                            child: Column(
                              children: [
                                ListTile(
                                  title: Text("${firestoreController.EventList[index].name}"),
                                  subtitle:Text(firestoreController.EventList[index].price.toString() +
                                      " \$",),
                                  trailing: const Icon(Icons.favorite_outline,color: Colors.redAccent,),
                                ),
                                Container(
                                  height: 200.0,
                                  child:
                                   Image.network(  "${firestoreController.EventList[index].image}",
                                     fit: BoxFit.cover,
                                     width: 500,
                                   )
                                ),
                                Container(
                                  padding: const EdgeInsets.all(16.0),
                                  alignment: Alignment.centerLeft,
                                  child:Text("${firestoreController.EventList[index].description}"),
                                ),
                                ButtonBar(
                                  children: [

                                    Container(
                                   decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(20),
                                color: Colors.white24,
                              ),
                              alignment: Alignment.center,
                                      child: TextButton(
                                        
                                        child: const Text('SEE MORE',style: TextStyle(color: Colors.blue),),
                                        onPressed: () { /** Details Page **/},
                                      ),
                                    )
                                  ],
                                ),

                              ],


                            )
                        );
                      }),
                )),
              ],
            ),
          ),
        ));
  }
}