import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:happyness/constance.dart';
import 'package:happyness/core/view_model/profile_view_model.dart';
import 'package:happyness/view/navigationdrawer.dart';
import 'package:happyness/view/widgets/custom_text.dart';

class ProfileView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<ProfileViewModel>(
        init: Get.put(ProfileViewModel()),
        builder: (controller) =>controller.loading.value
        ? const Center(
          child: CircularProgressIndicator(),
        )
            : Scaffold(
      appBar: AppBar(backgroundColor: primaryColor,  title: const Center(child: Text("HAPPY'NESS"),),),
      drawer: NavigationDrawer(),
      body: Container(
        padding: const EdgeInsets.only(top: 30),
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Container(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Container(
                      width: 120,
                      height: 120,
                      decoration:BoxDecoration(
                        color: Colors.red,
                        borderRadius: const BorderRadius.all(Radius.circular(100),
                        ),
                        image: DecorationImage(
                          image: controller.userModel!.pic != 'noImage'
                              ? NetworkImage(controller.userModel!.pic as String)
                              : const AssetImage('assets/avatar.png')  as ImageProvider,
                        fit: BoxFit.fill,
                        ),
                        ),),
                    Column(
                      children: [
                        CustomText(
                          text: controller.userModel!.name as String,
                          color: Colors.black,
                          fontSize: 32,
                        ),
                        CustomText(
                          text: controller.userModel!.email as String,
                          color: Colors.black,
                          fontSize: 24,
                        )

                      ],
                    )

                  ],
                ),

              ),
              const SizedBox(height: 50,),
              Container(
                child: FlatButton(
                  onPressed: (){},
                  child: ListTile(
                    title: CustomText(
                      text: 'Edit Profile',
                    ),
                    leading: const Icon(Icons.account_box,
                    ),
                    trailing: const Icon(Icons.navigate_next,color: Colors.black,),
                  ),

                ),
              ),
              const SizedBox(height: 20,),
              Container(
                child: FlatButton(
                  onPressed: (){},
                  child: ListTile(
                    title: CustomText(
                      text: 'Event History',
                    ),
                    leading: const Icon(Icons.history,
                    ),
                    trailing: const Icon(Icons.navigate_next,color: Colors.black,),
                  ),

                ),
              ),
              const SizedBox(height: 20,),
              Container(
                child: FlatButton(
                  onPressed: (){},
                  child: ListTile(
                    title: CustomText(
                      text: 'Reservation ',
                    ),
                    leading: const Icon(Icons.calendar_today,
                    ),
                    trailing: const Icon(Icons.navigate_next,color: Colors.black,),
                  ),

                ),
              ),
              const SizedBox(height: 20,),
              Container(
                child: FlatButton(
                  onPressed: (){},
                  child: ListTile(
                    title: CustomText(
                      text: 'Notifications ',
                    ),
                    leading: const Icon(Icons.notifications,
                    ),
                    trailing: const Icon(Icons.navigate_next,color: Colors.black,),
                  ),

                ),
              ),
              const SizedBox(height: 20,),
              Container(
                child: FlatButton(
                  onPressed: (){},
                  child: ListTile(
                    title: CustomText(
                      text: 'Log Out',
                    ),
                    leading: const Icon(Icons.logout,
                    ),
                    trailing: const Icon(Icons.navigate_next,color: Colors.black,),
                  ),

                ),
              ),
              const SizedBox(height: 20,),
            ],
          ),
        )
      ),
    )
    );
  }
}
