import 'package:flutter/material.dart';

import '../../constance.dart';
import 'custom_text.dart';

class CustomButton extends StatelessWidget {
  final String text;

  final Color color1;
  final Color color2;

  final VoidCallback? onPress;

  CustomButton({
    required this.onPress,
    this.text = '',
    this.color1 = primaryColor,
    this.color2 = Colors.white,
  });

  @override
  Widget build(BuildContext context) {
    return FlatButton(
      shape: new RoundedRectangleBorder(
        borderRadius: new BorderRadius.circular(10.0),
      ),
      padding: EdgeInsets.all(20),
      onPressed: onPress,
      color: color1,
      child: CustomText(
        fontWeight: FontWeight.w400,
        alignment: Alignment.center,
        text: text,
        color: color2,
      ),
    );
  }
}
