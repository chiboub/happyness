
import 'package:flutter/material.dart';
import 'package:happyness/constance.dart';




InputDecoration buildInputDecoration(IconData icons,String hinttext) {
  return InputDecoration(
    hintText: hinttext,
    prefixIcon: Icon(icons ,color: primaryColor),
    focusedBorder: OutlineInputBorder(
      borderRadius: BorderRadius.circular(25.0),
      borderSide:const  BorderSide(
          color: primaryColor,
          width: 1.5
      ),
    ),
    border: OutlineInputBorder(
      borderRadius: BorderRadius.circular(25.0),
      borderSide:const  BorderSide(
        color: Colors.grey,
        width: 1.5,
      ),
    ),
    enabledBorder:OutlineInputBorder(
      borderRadius: BorderRadius.circular(25.0),
      borderSide: const   BorderSide(
        color: primaryColor,//u  can change rom here
        width: 1.5,
      ),
    ),
  );
}