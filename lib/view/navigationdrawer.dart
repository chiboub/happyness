import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:happyness/view/auth/login_view.dart';
import 'package:happyness/view/categroy_view.dart';
import 'package:happyness/view/homee_view.dart';
import 'package:happyness/view/mode_view.dart';
import 'package:happyness/view/myevent_view.dart';
import 'package:happyness/view/profile_view.dart';

class NavigationDrawer extends StatelessWidget {
  const NavigationDrawer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Drawer(
     child: ListView(
       padding: EdgeInsets.zero,
       children: <Widget>[
         createDrawerHeader(),
         createDrawerBodyItem(
           icon: Icons.home,
           text: 'Home',
                    onTap:(){
             Get.to(HomeView());
                    }

         ),
         createDrawerBodyItem(
           icon: Icons.account_circle,
           text: 'Dashboard',
                 onTap:(){
             Get.to(ModeView());
                 }

         ),

         createDrawerBodyItem(
           icon: Icons.event_note,
           text: 'Events',
             onTap:(){
             Get.to(CategoryView());
             }

         ),
         createDrawerBodyItem(
             icon: Icons.category_sharp,
             text: 'Organization',
             onTap:(){
               Get.to(MyEventView());
             }

         ),
         Divider(),
         createDrawerBodyItem(
             icon: Icons.account_circle,
             text: 'Profile Info',
             onTap:(){
               Get.to(ProfileView());
             }

         ),
         createDrawerBodyItem(
           icon: Icons.notifications_active,
           text: 'Notifications',
             onTap:(){}

         ),
          Divider(),
           createDrawerBodyItem(
           icon: Icons.logout,
           text: 'Logout',
             onTap:(){
               Get.to(LoginView());
             }

         ),
         SizedBox(height: 80,),
         ListTile(
           title: const Text('Copyright © 2022 All rights reserved '),
           onTap: () {},
         ),
       ],
     ),
   );
 }
}
Widget createDrawerHeader() {
 return DrawerHeader(
     margin: EdgeInsets.zero,
     padding: EdgeInsets.zero,
     decoration: const BoxDecoration(
         image: DecorationImage(
             fit: BoxFit.fill,
             image:  AssetImage('assets/bg_header.jpeg'))),
     child: Stack(children: const <Widget>[
       Positioned(
           bottom: 12.0,
           left: 16.0,
           child: Text("Welcome to Happyness",
               style: TextStyle(
                   color: Colors.white,
                   fontSize: 20.0,
                   fontWeight: FontWeight.w500))),
     ]));
}
Widget createDrawerBodyItem(
   {IconData? icon, required String text, GestureTapCallback? onTap}) {
 return ListTile(
   title: Row(
     children: <Widget>[
       Icon(icon),
       Padding(
         padding: const EdgeInsets.only(left: 8.0),
         child: Text(text),
       )
     ],
   ),
   onTap: onTap,
 );
}