import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:get/get.dart';
import 'package:happyness/constance.dart';
import 'package:happyness/core/view_model/calendar_view_model.dart';
import 'package:happyness/view/navigationdrawer.dart';
import 'package:happyness/view/widgets/custom_text.dart';
import 'package:happyness/view/widgets/input_decoration.dart';


class MyEventView extends StatelessWidget {
  final CollectionReference _createCollectionRef =
  FirebaseFirestore.instance.collection('EventCreatedbyme');

  final _formkey = GlobalKey<FormState>();

  final TextEditingController _name = TextEditingController();
  final TextEditingController _capacity = TextEditingController();
  final TextEditingController _descripition = TextEditingController();
  final TextEditingController _image = TextEditingController();
  final TextEditingController _localisation = TextEditingController();
  final TextEditingController _price = TextEditingController();
  final TextEditingController _category = TextEditingController();
  final TextEditingController _date = TextEditingController();



  @override
  Widget build(BuildContext context) {
    return GetBuilder<CalendarViewModel>(
      init: Get.put(CalendarViewModel()),

      builder: (controller) => controller.loading.value
          ? const Center(child: CircularProgressIndicator())
          : Scaffold(
        appBar: AppBar(
          title: Center(child: Text("HAPPY'NESS"),),
          backgroundColor: primaryColor,
        ),
        drawer: const NavigationDrawer(),
        body: SingleChildScrollView(
          child: Container(
            padding: const EdgeInsets.only(top: 20, left: 20, right: 20),
            child: Column(
              children: [
                CustomText(
                  text: "EVENTS",
                  fontSize: 30,
                  fontWeight: FontWeight.bold,
                ),
                SizedBox(height: 10,),
                _listViewCalendar(),
              ],
            ),
          ),
        ),

        bottomNavigationBar: Padding(
          padding: const EdgeInsets.all(15.0),
          child: RaisedButton(
             
            color: primaryColor,
            onPressed: () {
              showDialog(

                  context: context,
                  builder: (BuildContext context) {
                    return Padding(
                      padding: const EdgeInsets.fromLTRB(20, 0, 20, 50),
                      child: Material(
                        color: Colors.grey.shade200,
                        elevation: 2,
                        shape:
                        RoundedRectangleBorder(borderRadius: BorderRadius.circular(32)),
                        child: SingleChildScrollView(

                            child: Padding(
                              padding:const  EdgeInsets.fromLTRB(20, 30, 20, 20),
                              child: Column(
                                children: <Widget>[
                                  Form(
                                      key: _formkey,
                                      child: Column(
                                        children: <Widget>[
                                          Container(
                                            alignment: Alignment.center,
                                            child: CustomText(
                                                    text: "CREATE NEW EVENT",
                                                    fontSize: 25 ,
                                            ),
                                          ),
                                          const SizedBox(height:10),
                                          TextFormField(
                                            controller: _name,
                                            decoration:  buildInputDecoration(Icons.title, "Event Title "),
                                            validator: (value) {
                                              if (value!.isEmpty) {
                                                return 'Please Fill Event Title Input';
                                              }
                                              // return 'Valid Title';
                                            },
                                          ),
                                        const SizedBox(
                                            height: 10,
                                          ),
                                          TextFormField(
                                            controller: _capacity,
                                            decoration:  buildInputDecoration(Icons.reduce_capacity, "Event Capacity "),
                                            validator: (value) {
                                              if (value!.isEmpty) {
                                                return 'Please Fill Event Capacity Input';
                                              }
                                              // return 'Valid Capacity';
                                            },
                                          ),
                                          const SizedBox(
                                            height: 10,
                                          ),
                                          TextFormField(
                                            controller: _price,
                                            decoration:  buildInputDecoration(Icons.price_change , "Event Price "),

                                            validator: (value) {
                                              if (value!.isEmpty) {
                                                return 'Please Fill Event Price Input';
                                              }
                                              // return 'Valid Price';
                                            },
                                          ),
                                          const SizedBox(
                                            height: 10,
                                          ),
                                          TextFormField(
                                            controller: _date,
                                            decoration:  buildInputDecoration(Icons.date_range , "Event date "),

                                            validator: (value) {
                                              if (value!.isEmpty) {
                                                return 'Please Fill Event date Input';
                                              }
                                              // return 'Valid date';
                                            },
                                          ),
                                          const SizedBox(
                                            height: 10,
                                          ),
                                          TextFormField(

                                            controller: _category,
                                            decoration:  buildInputDecoration(Icons.category , "Event category "),

                                            validator: (value) {
                                              if (value!.isEmpty) {
                                                return 'Please Fill Event category Input';
                                              }
                                              // return 'Valid category';
                                            },
                                          ),
                                          const SizedBox(
                                            height: 10,
                                          ),
                                          TextFormField(
                                            controller: _localisation,
                                            decoration:  buildInputDecoration(Icons.place , "Event location "),
                                            validator: (value) {
                                              if (value!.isEmpty) {
                                                return 'Please Fill Event location Input';
                                              }
                                              // return 'Valid location';
                                            },
                                          ),
                                          const SizedBox(
                                            height: 10,
                                          ),
                                          TextFormField(
                                            keyboardType: TextInputType.multiline,
                                            maxLines: 3,
                                            controller: _descripition,
                                             decoration:  buildInputDecoration(Icons.description, "Event Description "),
                                            validator: (value) {
                                              if (value!.isEmpty) {
                                                return 'Please Fill Event Description Input';
                                              }
                                            },
                                          ),
                                          const SizedBox(
                                            height: 20,
                                          ),
                                          Row(
                                            mainAxisAlignment: MainAxisAlignment.center,
                                            children: [
                                              RaisedButton(
                                                color: Colors.white,
                                                child: const Text(
                                                  'Cancel', style:  TextStyle(color: Colors.black),),
                                                onPressed: () {
                                                  Get.back();
                                                },
                                              ),

                                              const SizedBox(width: 20,),
                                              RaisedButton(
                                                color: primaryColor,
                                                child: const Text(
                                                  'Create', style: TextStyle(color: Colors.white),),
                                                onPressed: () async {
                                                  if (_formkey.currentState!.validate()) {
                                                    Map<String, dynamic> data = {
                                                      "name": _name.text,
                                                      "image": _image.text,
                                                      "localisation": _localisation.text,
                                                      "description": _descripition.text,
                                                      "price": _price.text,
                                                      "capacity": _capacity.text,
                                                      "category":_category.text,
                                                      "date":_date.text,
                                                    };
                                                    _createCollectionRef.add(data);
                                                    Get.to(MyEventView());
                                                  }
                                                },
                                              ),
                                            ],
                                          ),

                                        ],
                                      )
                                  )
                                ],
                              ),
                            ),

                        ),
                      ),
                    );
                  });
            },
              shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0),
                  ),
            child: const Text("Create new event",style:  TextStyle(color: Colors.white),),
          ),
        ),
      ),
    );
  }

  Widget _searchTextFormField() {
    return Container(
      
      decoration: BoxDecoration(
        
        borderRadius: BorderRadius.circular(20),
        color: Colors.grey.shade200,
      ),
      child: TextFormField(
        
        decoration: const InputDecoration(
          border: InputBorder.none,
          prefixIcon:  Icon(
            Icons.search,
            color: Colors.black,
          ),
          hintText: 'Search'
        ),
      ),
    );
  }



  Widget _listViewCalendar() {
    return GetBuilder<CalendarViewModel>(
      builder: (controller) => Container(
        height: 550,
        child: ListView.builder(
          scrollDirection: Axis.vertical,
          physics: const BouncingScrollPhysics(),
          shrinkWrap: true,
          itemCount: controller.calendarModel.length,

          itemBuilder: (context, index) {
            return GestureDetector(

              onTap: () {

              },
              child:  Column(
                children: [
                  Card(
                    color: Colors.grey.shade200,
                      elevation: 20.0,
                      child: Column(
                        children: [

                          Container(
                              height: 200.0,
                              child:
                              Image.network(  "${controller.calendarModel[index].image}",
                                fit: BoxFit.cover,
                                width: 500,
                              )
                          ),
                          ListTile(
                            title: Text("${controller.calendarModel[index].name}"),
                            subtitle:Text(controller.calendarModel[index].price.toString() +
                                " \$",),
                          ),
                          Container(
                            padding: const EdgeInsets.all(16.0),
                            alignment: Alignment.centerLeft,
                            child:Text("${controller.calendarModel[index].description}"),
                          ),


                              Container(
                                alignment: Alignment.center,
                                child: TextButton(

                                  child: const Text('SEE MORE'),
                                  onPressed: () {},
                                ),
                              )

                        ],
                      )),
                  const SizedBox(height: 20,)
                ],

              )

            );

          },

          ),

        ),

    );
  }



}

