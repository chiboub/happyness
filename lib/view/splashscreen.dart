import 'package:flutter/material.dart';
import 'package:happyness/view/mode_view.dart';

class SplashScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Stack(
          children: <Widget>[
            Container(
        decoration: const BoxDecoration(
        gradient:  LinearGradient(
        begin: Alignment.topRight,
          end: Alignment.bottomLeft,
          colors: [
            Colors.blue,
            Colors.red,
          ],
        )
    ),
           child: Container(
              padding: const EdgeInsets.symmetric(horizontal: 50),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Image.asset(
                    "assets/back.jpg",
                    height: 50,
                  ),
                  const SizedBox(height: 18,),
                  Row(
                    children: const <Widget>[
                       Text(
                        "HAPPY",
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 25,
                            fontWeight: FontWeight.w800),
                      ),
                      Text(
                        "NESS",
                        style: TextStyle(
                            color: Color(0xffFFA700),
                            fontSize: 25,
                            fontWeight: FontWeight.w800),
                      )
                    ],
                  ),
                  const SizedBox(height: 14,),
                  const Text("There’s a lot happening around you! Our mission is to provide what’s happening near you!",
                    style:  TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.w500
                    ),),
                  const SizedBox(height: 14,),
                  GestureDetector(
                    onTap: (){
                      Navigator.push(context, MaterialPageRoute(
                          builder: (context) => const ModeView()
                      ));
                    },
                    child: Container(
                      child: Row(
                        children: const <Widget>[
                          Text("Get Started", style: TextStyle(
                              color: Colors.white,
                              fontSize: 17
                          ),),
                          SizedBox(width: 5,),
                          Icon(Icons.arrow_forward, color: Colors.white,)
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ),
            )
          ],

        ));



  }
}