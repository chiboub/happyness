import 'package:get/get.dart';
import 'package:happyness/core/view_model/auth_view_model.dart';
import 'package:happyness/core/view_model/calendar_view_model.dart';
import 'package:happyness/core/view_model/reservation_view_model.dart';
import 'package:happyness/core/view_model/control_view_model.dart';
import 'package:happyness/core/view_model/home_view_model.dart';
import 'package:happyness/core/view_model/profile_view_model.dart';

class Binding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => AuthViewModel());
    Get.lazyPut(() => ControlViewModel());
    Get.lazyPut(() => HomeViewModel());
    Get.lazyPut(() => ReservationViewModel());
    Get.lazyPut(() => CalendarViewModel());
    Get.lazyPut(() => ProfileViewModel());

  }
}
